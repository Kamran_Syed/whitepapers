<?php
/*
Plugin Name: White Papers
Plugin URI: 
Description: Allows users to download White papers from World Franchising site.
Version: 2.1
Author: AgileSolutionsPK
Author URI: http://agilesolutionspk.com
*/

if ( !class_exists( 'WC_White_Papers' )){   
	class WC_White_Papers{

		function __construct() {
			register_activation_hook( __FILE__, array(&$this, 'install') );
			add_action('admin_menu', array(&$this,'admin_menu'));
			add_action('admin_init', array(&$this,'admin_init'));
			add_action('admin_enqueue_scripts', array(&$this, 'admin_enqueue_scripts') );
			add_action('wp_enqueue_scripts', array(&$this, 'frontend_enqueue_scripts') );
			add_shortcode('show_optin_form', array(&$this,'show_optin_form'));
			add_action( 'wp_ajax_aspk_download', array( $this, 'download_pdf' ) );
			add_action( 'wp_ajax_nopriv_aspk_download', array( $this, 'download_pdf' ) );
		}
		
		
		function admin_init(){
			ob_start();
		}
		
		function download_csv($ret){
			ob_end_clean();
			
			header("Content-type: text/csv");
			header("Content-Disposition: attachment; filename=Subscriber-Report.csv");
			header("Pragma: no-cache");
			header("Expires: 0");
			
			$output = fopen("php://output", "w");
			
			foreach($ret as $r){
				$tmpdt = new DateTime($r->dt);
				$r->dt = $tmpdt->format('m/d/Y');
				$row = (array)$r;
				fputcsv($output, $row, ",");
			}
			
			fclose($output);
			exit;
		}
		
		
		function download_pdf(){
			
			check_ajax_referer( 'aspk_download' );
			$filename = urldecode($_GET['filename']);
			$upload_array = wp_upload_dir();
			$path=$upload_array['basedir'].'/files/1/'.$filename; 
			
			if(! is_file( $path )){
				echo "<div>Sorry, File is not Available, please contact us</div>";
				exit;
			}
			
			header('Content-Transfer-Encoding: binary');  // For Gecko browsers mainly
			header('Last-Modified: ' . gmdate('D, d M Y H:i:s', filemtime($path)) . ' GMT');
			header('Accept-Ranges: bytes');  // For download resume
			header('Content-Length: ' . filesize($path));  // File size
			header('Content-Encoding: none');
			header('Content-Type: application/pdf');  // Change this mime type if the file is not PDF
			header('Content-Disposition: attachment; filename=' . $filename);  // Make the browser display the Save As dialog
			readfile($path);  //this is necessary in order to get it to actually download the file, otherwise it will be 0Kb
			exit;
		}
		
		
		function install() {
			
			global $wpdb;
			
			$sql = "CREATE TABLE IF NOT EXISTS `".$wpdb->prefix."white_papers` (
			  `id` int(10) NOT NULL auto_increment,
			  `list_name` varchar(30) NOT NULL,
			  `first_name` varchar(30) NOT NULL,
			  `last_name` varchar(30) NOT NULL,
			  `company` varchar(30) NOT NULL,
			  `title` varchar(30) NOT NULL,
			  `email` varchar(30) NOT NULL,
			  `dt` datetime NULL,
			  PRIMARY KEY  (`id`)
			)";
			$wpdb->query($sql);
			
		}
		
		function admin_enqueue_scripts(){
			wp_enqueue_script('jquery');
			wp_enqueue_script('jquery-ui-core');
			wp_enqueue_style( 'tw-bs_bootstrap', plugins_url('css/bootstrap.min.css', __FILE__) );
			wp_enqueue_style( 'whitepapers', plugins_url('css/whitepapers.css', __FILE__) );
			wp_enqueue_script('bootstrap-js', plugins_url('js/bootstrap.min.js', __FILE__));
		}
		
		function frontend_enqueue_scripts(){
			wp_enqueue_script('jquery');
			//wp_enqueue_script('jquery-ui-core');
			wp_enqueue_style( 'tw-bs_bootstrap', plugins_url('css/bootstrap.min.css', __FILE__) );
			wp_enqueue_style( 'whitepapers', plugins_url('css/whitepapers.css', __FILE__) );
			wp_enqueue_script('bootstrap-js', plugins_url('js/bootstrap.min.js', __FILE__));
			wp_enqueue_script( 'no_captcha_login', 'https://www.google.com/recaptcha/api.js' );

		}
		
		function show_optin_form($atts){
			
			$a = shortcode_atts( array(
				'filename' => '1',
				'listname' => 'auto'
			), $atts );
			
			$content = $this->handle_optin_form($a);
			if($content){
				return $content;
			}
			ob_start();
		?>
			<script>
				function validateEmail($email) {
					var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
					return emailReg.test( $email );
				}
				
				function letTheFormGo(){
					jQuery('input').removeClass("mustFill");
					jQuery('.modal-title').text('Download Whitepapers');

					if(jQuery('#firstname').val().length < 3){
						jQuery('#firstname').addClass("mustFill");
						jQuery('.modal-title').text('Please fill in field marked in RED');
						return false;
					}else if(jQuery('#lastname').val().length < 3){
						jQuery('#lastname').addClass("mustFill");
						jQuery('.modal-title').text('Please fill in field marked in RED');
						return false;
					}else if(jQuery('#company').val().length < 3){
						jQuery('#company').addClass("mustFill");
						jQuery('.modal-title').text('Please fill in field marked in RED');
						return false;
					}else if(jQuery('#title').val().length < 2){
						jQuery('#title').addClass("mustFill");
						jQuery('.modal-title').text('Please fill in field marked in RED');
						return false;
					}else if(!validateEmail( jQuery('#email').val() ) || (jQuery('#email').val().length < 5)){
						jQuery('#email').addClass("mustFill");
						jQuery('.modal-title').text('Please fill in field marked in RED');
						return false;
					}
					
				}
			</script>
					<div class="modal-content modal-sm" style="min-width:345px;text-align:center;">
						<div class="modal-header">
							<h4 class="modal-title">Download Whitepapers</h4>
						</div>
						<div class="modal-body">
							<form name="dontcare"  method="post" >
								<div class = "form-group">
									<label for="firstname">First Name:</label>
									<input class = "form-control" type = "text" name = "firstname" id = "firstname" value = "" />
								</div>
								<div class = "form-group">
									<label for="lastname">Last Name:</label>
									<input class = "form-control" type = "text" name = "lastname" id = "lastname" value = ""  />
								</div>
								<div class = "form-group">
									<label for="company">Company:</label>
									<input class = "form-control" type = "text" name = "company" id = "company" value = ""  />
								</div>
								<div class = "form-group">
									<label for="title">Title:</label>
									<input class = "form-control" type = "text" name = "title" id = "title" value = ""   />
								</div>
								<div class = "form-group">
									<label for="email">Email:</label>
									<input class = "form-control" type = "text" name = "email" id = "email" value = ""  />
								</div>
								<?php wr_no_captcha_render_login_captcha();?>
								<div class="modal-footer">
									<input type="submit" class="btn btn-primary" style="margin-top:1em" name="submit" value="Download Now" onclick="return letTheFormGo();" />
									<button type="button" class="btn btn-default" style="margin-top:1em" data-dismiss="modal">Cancel</button>
								</div>
							</form>
						</div>
					</div>

		
		<?php
			return ob_get_clean();
		}	
		
		function handle_optin_form($a){ //$a is array of attributes
			global $wpdb;
			
			if(! isset($_POST['submit'])) return false;
			
			if ( isset( $_POST['g-recaptcha-response'] ) ) {
				$no_captcha_secret = get_option( 'wr_no_captcha_secret_key' );
				$response = wp_remote_get( 'https://www.google.com/recaptcha/api/siteverify?secret=' . $no_captcha_secret . '&response=' . $_POST['g-recaptcha-response'] );
				$response = json_decode( $response['body'], true );
				if ( true === $response['success'] ) {
					//do nothing
				} else {
					return "<script>jQuery(document).ready(function(){jQuery('.modal-title').text('Sorry, Captcha is incorrect');});</script>";
				}
			}else{
				return "<script>jQuery(document).ready(function(){jQuery('.modal-title').text('Sorry, Captcha is incorrect');});</script>";
			}
			
			$sql = "INSERT INTO ".$wpdb->prefix."white_papers SET list_name = '{$a['listname']}',first_name = '{$_POST['firstname']}', last_name = '{$_POST['lastname']}', company = '{$_POST['company']}', title = '{$_POST['title']}', email = '{$_POST['email']}',dt = now()";
			
			
			$wpdb->query($sql);
			$tmpurl = admin_url( 'admin-ajax.php');
			$url = wp_nonce_url( $tmpurl, 'aspk_download' ). '&action=aspk_download&filename='.$a['filename']; 
			$msg = '<div style="text-align:center;"><h2>Thank you for downloading '.$a['filename'].'</h2></div>';
			$msg .= '<script>window.location.href="'.$url.'"</script>';
			
			return $msg;
		}
		
		function admin_menu() {
			add_menu_page('Whitepapers Report', 'Whitepapers Report', 'manage_options', 'qna_wc_whitepapers_report', array(&$this, 'admin_report_page'));
		}
		
		function admin_report_page() {
			global $wpdb;
			
			if(isset($_POST['whitepapers_report'])){
				//nothing here
			}else if(isset($_POST['whitepapers_csv'])){
				if(!isset($_POST['report_period'])) $_POST['report_period'] = 7;
				$ret = $this->get_report_data($_POST['report_period']);
				if($ret) $this->download_csv($ret);
				
			}else{
				$_POST['report_period'] = 7;
			}
			
			$records = $this->get_report_data($_POST['report_period']);
			
			?>
			<div style="margin-left:1em;"><h1>Report of Subscribers</h1></div>
			<form method="post" action="">
				<div class="row">
					
					<div class="col-md-3 col-md-offset-1">
						<div class = "form-group">
							<label>Report Period : </label> <!--options last 7 days, 1month, 3 months,  6 months, 1 year, 1 years  values must be in days-->
							<select name="report_period">
							  <option value="7">7 days</option>
							  <option value="30">1 month</option>
							  <option value="90">3 months</option>
							  <option value="180">6 months</option>
							  <option value="365">1 year</option>
							  <option value="730">1 years</option>
							</select> 
						</div>
					</div>
					<div class="col-md-8"></div>
				</div>
				<div class="row">
					<div class="col-md-2 col-md-offset-1">
						<input type="submit" name="whitepapers_report" value="Get Report" />
					</div>
					<div class="col-md-2">
						<input type="submit" name="whitepapers_csv" value="Download CSV" />
					</div>
					<div class="col-md-7"></div>
				</div>
			</form>
			
			
			 <div class="container aspk_container" id="report_container" >
				
				<div class="row">
					<div class="col-md-2 aspk-col-hdr" >Blast Name</div>
					<div class="col-md-1 aspk-col-hdr" >First Name</div>
					<div class="col-md-1 aspk-col-hdr" >Last Name</div>
					<div class="col-md-3 aspk-col-hdr" >Company</div>
					<div class="col-md-1 aspk-col-hdr" >Title</div>
					<div class="col-md-3 aspk-col-hdr" >Email</div>
					<div class="col-md-1 aspk-col-hdr" >Date</div>
				</div>
				
				
				<?php
				
					if($records){
						foreach($records as $r){
							$dt = new DateTime($r->dt);
							?>
							<div class="row">
								<div class="col-md-2 aspk-col-data" ><?php echo $r->list_name;?></div>
								<div class="col-md-1 aspk-col-data" ><?php echo $r->first_name;?></div>
								<div class="col-md-1 aspk-col-data" ><?php echo $r->last_name;?></div>
								<div class="col-md-3 aspk-col-data" ><?php echo $r->company;?></div>
								<div class="col-md-1 aspk-col-data" ><?php echo $r->title;?></div>
								<div class="col-md-3 aspk-col-data" ><?php echo $r->email;?></div>
								<div class="col-md-1 aspk-col-data" ><?php echo $dt->format('m/d/Y');?></div>
							</div>
							<?php
						}
					}
				?>
				
			 </div>
			<?php
			
			
		}
		
		function get_report_data($select_days){
			global $wpdb;

			$dt = new DateTime(); //current date time
			$dt->sub(new DateInterval('P'.$select_days.'D'));
			$start_date = $dt->format('Y-m-d h:i:s');
			
			$sql = "select * from ".$wpdb->prefix."white_papers where dt > '".$start_date."' order by dt desc";
			$ret = $wpdb->get_results($sql);
			
			return $ret;
		}
		
	}//class ends
}//existing class ends

new WC_White_Papers();